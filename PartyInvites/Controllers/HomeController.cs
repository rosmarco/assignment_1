﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PartyInvites.Models;

namespace PartyInvites.Controllers
{
    // the Home Controller is responsible for multiple views
    public class HomeController : Controller
    {
        public ViewResult Index()
        {
            int hour = DateTime.Now.Hour;
            ViewBag.Greeting = hour < 12 ? "Good Morning" : "Good Afternoon";
            return View();
        }

        [HttpGet]
        public ViewResult RsvpForm()
        {
            return View();
        }

        // using Request.Form, the RsvpForm decides which cshtml page to send the user to based on which button they pressed
        // Accept Invitation takes them to the Thanks.cshtml while the Send Regrets button takes them to the Regrets.cshtml page
        [HttpPost]
        public ViewResult RsvpForm(GuestResponse guestResponse) {

            if (ModelState.IsValid) {
                if (Request.Form["submit"] == "Accept Invitation")
                {
                    return View("Thanks", guestResponse);
                }
                else if (Request.Form["submit"] == "Send Regrets")
                {
                    return View("Regrets", guestResponse);
                }
                else
                {
                    return View();
                }
            }
            else
            {
                return View();
            }
        }
    }
}